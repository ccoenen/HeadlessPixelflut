import { Canvas } from './lib/Canvas.js';
import { FixedRateCallback } from './lib/FixedRateCallback.js';
import { PixelflutServer } from './lib/PixelflutServer.js';
import { StatsCollector } from './lib/StatsCollector.js';
import { VideoOutput } from './lib/VideoOutput.js';
import { Webserver } from './lib/Webserver.js';

import { logger } from './lib/logger.js';
import config from './config.js';

logger.info('HeadlessPixelflut starting');

const canvas = new Canvas(config.canvas.width, config.canvas.height);
const pixelflutserver = new PixelflutServer(config.pixelflut.port, config.canvas.width, config.canvas.height);
const statsCollector = new StatsCollector(pixelflutserver);
const videoOutput = new VideoOutput(config);
const webserver = new Webserver(config.webserver);

pixelflutserver.on('instruction', (instruction) => {
	canvas.draw(instruction);
});

canvas.autoStoreInterval(config.stillImageInterval);
videoOutput.autoRestartInterval(6 * 60 * 60 * 1000);

const imageCacheInterval = new FixedRateCallback(() => {
	webserver.updateImageCache(canvas.getPNG());
}, config.webserverViewInterval);

const videoframeInterval = new FixedRateCallback(() => {
	videoOutput.write(canvas.canvas);
	statsCollector.videoFramesCreated++;
}, 1000 / (config.videoDestination.framerate || 25));

statsCollector.emitStatsInterval(10000);

statsCollector.on('stats', (stats) => {
	logger.info(`on average ${Math.round(stats.pixelsPerSecond)} pixels per second from ${pixelflutserver.streams.length} connections`);
	webserver.updateStats({ ...stats, ...videoOutput.mostRecentStats });
});

process.on('SIGINT', () => {
	logger.warn('Gracefully shutting down from SIGINT');
	imageCacheInterval.stop();
	videoframeInterval.stop();
	canvas.stopAutoStoreInterval();
	statsCollector.stopEmitStatsInterval();
	videoOutput.close();
	videoOutput.stopAutoRestartInterval();
	pixelflutserver.close();
	webserver.close();
});
