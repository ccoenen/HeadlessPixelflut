import { mkdirSync } from 'fs';
import { resolve } from 'path';

import { DateTime } from 'luxon';

export class FilenameGenerator {
	constructor(basepath, extension) {
		this.basepath = basepath;
		this.extension = extension;
	}

	prepareName() {
		const now = DateTime.now();
		const dir = resolve(this.basepath, now.toISODate());
		mkdirSync(dir, { recursive: true });
		return resolve(dir, `${now.ts}.${this.extension}`);
	}
}
