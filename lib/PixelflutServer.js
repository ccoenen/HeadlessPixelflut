import { createServer } from 'net';
import EventEmitter from 'events';

import { StreamInterpreter } from './StreamInterpreter.js';
import { logger } from './logger.js';

export class PixelflutServer extends EventEmitter {
	constructor(port = 1234, width = 0, height = 0) {
		super();

		this.streamCounter = 0;
		this.streams = [];
		this.port = port;
		this.canvasSize = { width, height };

		this.socket = createServer(c => this.addStream(c));

		this.socket.on('error', (error) => {
			logger.error(`pixelflut server error ${error}`);
		});

		this.socket.on('close', (args) => {
			logger.warn(`no longer listening on pixelflut port ${this.port}`);
			this.emit('close', args);
		});

		this.socket.listen(this.port, () => {
			logger.info(`listening on pixelflut port ${this.port}`);
			this.emit('listening');
		});
	}

	addStream(stream) {
		this.emit('add-stream');
		this.streamCounter++;
		const si = new StreamInterpreter(this.streamCounter, stream);
		si.setCanvasSize(this.canvasSize.width, this.canvasSize.height);
		logger.info(`[${si.id}] + added stream from ${stream.remoteAddress}:${stream.remotePort}`);
		this.streams.push(si);

		si.on('instruction', instruction => this.emit('instruction', instruction));
		si.on('unhandled-message', () => this.emit('unhandled-message'));
		si.on('size', () => this.emit('size'));
		si.once('close', () => this.removeStream(si));
	}

	removeStream(si) {
		this.emit('remove-stream');
		logger.info(`[${si.id}] - removed stream`);
		const index = this.streams.indexOf(si);
		if (index < 0) {
			throw `stream ${si.id} was not found?!`;
		}
		this.streams.splice(index, 1);
		si.removeAllListeners('close');
	}

	close() {
		this.streams.forEach(s => {
			s.close();
		});
		this.socket.close();
	}
}
