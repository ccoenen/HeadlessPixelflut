import readline from 'readline';
import EventEmitter from 'events';

import { logger } from './logger.js';
import { Instruction } from './Instruction.js';

export class StreamInterpreter extends EventEmitter {
	constructor(id, stream) {
		super();
		this.id = id;
		this.stream = stream;
		this.errorcounter = 0;
		this.canvas = { width: 0, height: 0 };

		this.rl = readline.createInterface({
			input: this.stream,
			output: this.stream
		});

		this.rl.on('line', l => this.line(l));

		this.stream.on('error', (err) => {
			logger.info(`[${this.id}] Error in stream. Reason: ${err}`);
		});
		this.stream.on('close', (args) => {
			logger.info(`[${this.id}] - stream closed.`);
			this.emit('close', args);
			this.close();
		});
	}

	line(line) {
		const [instruction, reason] = Instruction.fromLineViaSplit(line);
		if (instruction) {
			this.emit('instruction', instruction);
		} else if (line === 'SIZE') {
			this.emit('size', this);
			this.stream.write(`SIZE ${this.canvas.width} ${this.canvas.height}\n`);
		} else if (this.errorcounter < 3) {
			this.errorcounter++;
			this.emit('unhandled-message', line, reason);
			logger.warn(`[${this.id}] could not decipher message ${line}`);
			if (this.stream.writable) {
				this.stream.write(`could not understand your last request: "${line}"\nthis is the parser error: ${reason}\n`);
			}
		} else {
			if (this.stream.writable) {
				this.stream.end('Too many errors on this stream. Bye\n');
			}
			logger.warn(`[${this.id}] too many errors on stream ${line}`);
		}
	}

	setCanvasSize(width, height) {
		this.canvas = {	width, height };
	}

	close() {
		if (this.stream) { this.stream.end(); }
		if (this.rl) { this.rl.close(); }
		this.removeAllListeners('instruction');
		this.removeAllListeners('size');
		this.removeAllListeners('unhandled-message');
		this.stream = null;
		this.rl = null;
	}
}
