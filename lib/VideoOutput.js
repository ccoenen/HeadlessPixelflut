import { spawn } from 'child_process';

import winston from 'winston';

import { FilenameGenerator } from './FilenameGenerator.js';
import { FixedRateCallback } from './FixedRateCallback.js';

import { logger as generalLogger } from './logger.js';


export const FFMPEG_STDERR_STATS_LINE = /^((?<key>\w+)=\s*(?<value>[\d:.]+|N\/A)[\w/]*\s*)+$/i;


export const FFMPEG_STDERR_STATS_SINGLE = /(?<key>\w+)=\s*(?<value>[\d:.]+)[\w/]*\s*/gi;


export function ffmpegStderrStatsToObject(line) {
	const generalMatch = line.match(FFMPEG_STDERR_STATS_LINE);
	if (!generalMatch) {
		return null;
	}

	const deconstructed = {};
	for (const match of line.matchAll(FFMPEG_STDERR_STATS_SINGLE)) {
		let value;
		if (match.groups.key === 'time') {
			value = match.groups.value.split(':').reverse().reduce((accumulator, item, index) => {
				return accumulator + parseFloat(item) * Math.pow(60, index);
			}, 0);
		} else {
			value = parseFloat(match.groups.value);
		}
		parseFloat(match.groups.value);
		if (!isNaN(value)) {
			deconstructed['ffmpeg.' + match.groups.key] = value;
		}
	}

	return deconstructed;
}


export class VideoOutput {
	constructor(config) {
		this.config = config;
		this.filenameGenerator = new FilenameGenerator('public/stored/video', 'mkv');
		this.process = null;
		this.fileLogger = null;
		this.mostRecentStats = null;
		this.createProcess();
	}


	constructFfmpegArgs(timelapseVideoFile) {
		// store a file
		// this.process = spawn('ffmpeg', ['-f', 'rawvideo', '-pixel_format', 'rgb0', '-video_size', `${config.canvas.width}x${config.canvas.height}`, '-framerate', config.playbackFramerate, '-i', '-', outputTarget]);
		const dualOutputs = (timelapseVideoFile && this.config.videoDestination.rtmpURL);

		const ffmpegInputArgs = [
			// we're piping raw bytes in here in rgba format.
			'-f', 'rawvideo',
			'-pixel_format', 'rgb0',
			'-video_size', `${this.config.canvas.width}x${this.config.canvas.height}`,
			'-i', '-',
		];

		const ffmpegRTMPArgs =[
			'-vcodec', 'libx264',
			'-profile:v', 'high',
			'-preset', 'veryfast',
			'-crf', '25',
			'-g', `${this.config.videoDestination.framerate * 2}`, // group of pictures forces an I-Frame every so-and-so frames.
			'-f', 'flv',
			'-flvflags', 'no_duration_filesize',
			this.config.videoDestination.rtmpURL,
		];

		const ffmpegFileArgs = [
			'-vcodec', 'libx264',
			'-profile:v', 'high',
			'-preset', 'medium',
			'-crf', '22',
			timelapseVideoFile
		];

		// select filter for skipping some frames: https://ffmpeg.org/ffmpeg-filters.html#select_002c-aselect
		// speeding up via setpts filter: https://trac.ffmpeg.org/wiki/How%20to%20speed%20up%20/%20slow%20down%20a%20video
		// other explored ways:
		// - framestep=100 (but plays slowly, then because PTS are not changed.)
		// - select='not(mod(n\,100))' (same as framestep above)
		const ffmpegGlueArgs = ['-filter_complex', 'format=pix_fmts=yuv420p'];
		if (dualOutputs) {
			// complex filter for multiple outputs: https://trac.ffmpeg.org/wiki/Creating%20multiple%20outputs
			ffmpegGlueArgs[1] += `,split[diskskip][stream];[diskskip]setpts=${this.config.videoDestination.storedVideoTimelapseFactor}*PTS[disk]`;
			ffmpegRTMPArgs.unshift('-map', '[stream]');
			ffmpegFileArgs.unshift('-map', '[disk]');
		} else if (timelapseVideoFile) {
			ffmpegGlueArgs[1] += `,setpts=${this.config.videoDestination.storedVideoTimelapseFactor}*PTS`;
		}

		const ffmpegArgs = [];
		ffmpegArgs.push(...ffmpegInputArgs, ...ffmpegGlueArgs);
		if (this.config.videoDestination.rtmpURL) {
			ffmpegArgs.push(...ffmpegRTMPArgs);
		}
		if (timelapseVideoFile) {
			ffmpegArgs.push(...ffmpegFileArgs);
		}
		return ffmpegArgs;
	}


	createProcess() {
		if (this.process && this.process.exitCode === null) {
			generalLogger.error(`attempting to create a new process while another process is still not exited! Old PID: #${this.process.pid}`);
			this.close();
		}
		const timelapseVideoFile = this.filenameGenerator.prepareName();
		const ffmpegArgs = this.constructFfmpegArgs(timelapseVideoFile);
		this.logger = winston.createLogger({
			level: 'debug',
			format: winston.format.simple(),
			transports: [ new winston.transports.File({ filename: timelapseVideoFile+'.log' }) ]
		});
		generalLogger.info(`Starting up FFMPEG with these arguments: ${ffmpegArgs.join(' ')}`);
		this.logger.info(`Starting up FFMPEG with these arguments: ${ffmpegArgs.join(' ')}`);
		this.mostRecentStats = {};
		this.process = spawn('ffmpeg', ffmpegArgs, { detached: true });
		generalLogger.info(`New FFMPEG process has PID #${this.process.pid}`);
		this.logger.info(`New FFMPEG process has PID #${this.process.pid}`);
		this.addProcessEventListeners();
	}


	addProcessEventListeners() {
		this.process.stdout.on('data', (data) => {
			this.logger.verbose(`ffmpeg-o #${this.process.pid}: ${data}`);
		});
		this.process.stderr.on('data', (data) => {
			this.logger.verbose(`ffmpeg-e #${this.process.pid}: ${data}`);
			const statsline = ffmpegStderrStatsToObject(data.toString());
			if (statsline) {
				this.mostRecentStats = statsline;
			}
		});
		this.process.stdin.on('error', (err) => {
			generalLogger.error(`ffmpeg-i #${this.process.pid}: error while writing to stdin: ${err}`);
			this.logger.error(`ffmpeg-i #${this.process.pid}: error while writing to stdin: ${err}`);
		});
		this.process.on('exit', (code) => {
			generalLogger.info(`ffmpeg #${this.process.pid}: exited with code ${code}`);
			this.logger.info(`ffmpeg #${this.process.pid}: exited with code ${code}`);
		});
		this.process.on('error', (err) => {
			generalLogger.error(`ffmpeg #${this.process.pid}: exited with this error ${err}`);
			this.logger.error(`ffmpeg #${this.process.pid}: exited with this error ${err}`);
		});
	}


	close() {
		this.process.stdin.end();
	}


	autoRestartInterval(interval_ms) {
		this.interval = new FixedRateCallback(() => {
			this.process.once('exit', (code) => {
				generalLogger.info(`ffmpeg #${this.process.pid} exited from autoRestartInterval with exitcode ${code}.`);
				this.createProcess();
			});
			generalLogger.info(`attempting to stop ffmpeg #${this.process.pid} from autoRestartInterval.`);
			this.close();
		}, interval_ms);
	}


	stopAutoRestartInterval() {
		if (this.interval) {
			this.interval.stop();
		}
	}


	write(data) {
		if (this.process.stdin.writable) {
			this.process.stdin.write(data);
		}
	}
}
