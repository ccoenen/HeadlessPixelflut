import EventEmitter from 'events';
import { performance } from 'perf_hooks';


export class StatsCollector extends EventEmitter {
	constructor(pixelflutserver) {
		super();
		this.pixelflutserver = pixelflutserver;

		this.pixelflutserver.on('instruction', () => {
			this.pixels++;
		});
		this.pixelflutserver.on('size', () => {
			this.sizeMessages++;
		});
		this.pixelflutserver.on('unhandled-message', () => {
			this.unhandledMessages++;
		});

		this.pixelflutserver.on('add-stream', () => {
			this.addedStreams++;
		});

		this.pixelflutserver.on('remove-stream', () => {
			this.removedStreams++;
		});

		this.reset();
	}


	reset() {
		this.pixels = 0;
		this.sizeMessages = 0;
		this.unhandledMessages = 0;

		this.addedStreams = 0;
		this.removedStreams = 0;

		this.videoFramesCreated = 0;

		this.resetTime = performance.now();
	}

	
	getCurrentStats() {
		const seconds = (performance.now() - this.resetTime) / 1000;

		return {
			activeStreams: this.pixelflutserver.streams.length,
			totalStreams: this.pixelflutserver.streamCounter,
			uptime: performance.now() / 1000,

			pixelsPerSecond: this.pixels / seconds,
			videoFramesCreatedPerSecond: this.videoFramesCreated / seconds,
			sizeMessagesPerSecond: this.sizeMessages / seconds,
			unhandledMessagesPerSecond: this.unhandledMessages / seconds,
			addedStreamsPerSecond: this.addedStreams / seconds,
			removedStreamsPerSecond: this.removedStreams / seconds,
		};
	}


	emitStatsInterval(timeframe) {
		this.emitInterval = setInterval(() => {
			this.emit('stats', this.getCurrentStats());
			this.reset();
		}, timeframe);
	}


	stopEmitStatsInterval() {
		clearInterval(this.emitInterval);
		this.emitInterval = null;
	}
}
