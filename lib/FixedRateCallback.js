export class FixedRateCallback {
	constructor(callback, interval) {
		this.callback = callback;
		this.interval = interval;
		this.callAndReset();
	}

	callAndReset() {
		// const iterationTimeout = this.interval - (performance.now() % this.interval);
		// console.log(`FixedRateCallback, iteration timeout: ${iterationTimeout}`);
		const iterationTimeout = this.interval;

		this.timeout = setTimeout(() => {
			this.callAndReset();
			this.callback.call();
		}, iterationTimeout);
	}

	stop() {
		clearTimeout(this.timeout);
	}
}
