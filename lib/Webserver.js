import express from 'express';

import { logger } from './logger.js';

export class Webserver {
	constructor(config) {
		this.imageCache;
		this.statsCache;
		this.app = express();
		this.app.use(express.static('public'));
		this.addRoutes();
		this.server = this.app.listen(config.port, () => { logger.info(`webserver listening on ${config.port}`); });

		this.server.on('close', () => { logger.warn('webserver no longer listening.'); });
	}

	addRoutes() {
		this.app.get('/image.png', (req, res) => {
			res.type('png');
			res.end(this.imageCache);
		});

		this.app.get('/stats', (req, res) => {
			res.json(this.statsCache);
		});
	}

	updateImageCache(data) {
		this.imageCache = data;
	}

	updateStats(data) {
		this.statsCache = data;
	}

	close() {
		this.server.close();
	}
}
