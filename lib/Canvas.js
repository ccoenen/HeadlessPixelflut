import fs from 'fs';

import UPNG from '@pdf-lib/upng';

import { logger } from './logger.js';
import { FilenameGenerator } from './FilenameGenerator.js';

export class Canvas {
	constructor(width, height) {
		this.width = width;
		this.height = height;
		this.canvas = new Uint8Array(width * height * 4);
		this.fill(0, 0, 0, 255); // @see https://www.youtube.com/watch?v=O4irXQhgMqg
		this.filenameGenerator = new FilenameGenerator('public/stored/still-images', 'png');
	}


	fill(r = 0, g = 0, b = 0, a = 255) {
		for (let i = 0; i < this.canvas.length; i += 4) {
			this.canvas[i] = r;
			this.canvas[i + 1] = g;
			this.canvas[i + 2] = b;
			this.canvas[i + 3] = a;
		}
	}


	draw(instruction) {
		const offset = (instruction.x + instruction.y * this.width) * 4;
		const canvas = this.canvas;
		canvas[offset + 0] = instruction.r;
		canvas[offset + 1] = instruction.g;
		canvas[offset + 2] = instruction.b;
		canvas[offset + 3] = 0xFF; // FF => fully opaque.
	}


	getPNG() {
		return new Uint8Array(UPNG.default.encode([this.canvas], this.width, this.height, 0));
	}


	async store(filename) {
		return new Promise((resolve) => {
			const out = fs.createWriteStream(filename);
			out.end(this.getPNG(), 'binary');
			out.once('finish', resolve);
		});
	}


	autoStoreInterval(milliseconds) {
		clearInterval(this.autoStoreInterval);
		this.autoStoreInterval = setInterval(async () => {
			const imagefile = this.filenameGenerator.prepareName();
			logger.info(`storing current image in ${imagefile}`);
			await this.store(imagefile);
		}, milliseconds);
	}


	stopAutoStoreInterval() {
		clearInterval(this.autoStoreInterval);
		this.autoStoreInterval = null;
	}
}
