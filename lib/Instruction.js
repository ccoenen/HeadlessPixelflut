export const FORMAT_PARSER = /^PX\s+(?<x>\d+)\s+(?<y>\d+)\s+(?<r>[0-9a-f]{2})(?<g>[0-9a-f]{2})(?<b>[0-9a-f]{2})\s*$/i;

export class Instruction {
	static fromLineViaRegex(input) {
		const result = input.match(FORMAT_PARSER);
		if (!result) return [null, 'did not match the format'];
		const groups = result.groups;
		return [new Instruction(parseInt(groups.x, 10), parseInt(groups.y, 10), parseInt(groups.r, 16), parseInt(groups.g, 16), parseInt(groups.b, 16)), null];
	}


	/**
	 * parses a line using a String#split, which is faster than the regular expression
	 * @param {String} input whatever we receive from a source
	 * @returns [instruction, reason], instruction is an instance of Instruction or `null`. If parsing fails, `reason` will be a string with a human-readable error.
	 */
	static fromLineViaSplit(input) {
		if (input.length > 19) return [null, 'Message is too long. Did you forget to add newlines at the end?']; // allows up to 9999x9999 pixels :-D
		const result = input.split(' ');
		if (result.length !== 4) return [null, 'Message has too many parts. There should be four parts: PX X Y COLOR.'];
		if (result[0] !== 'PX') return [null, `The first part should always be uppercase "PX" but it is ${result[0]}.`];
		if (result[3].length !== 6) return [null, `The last part should always be six hex-digits for the color, but it is ${result[3]}.`];
		const x = parseInt(result[1], 10);
		const y = parseInt(result[2], 10);
		const r = parseInt(result[3].slice(0,2), 16);
		const g = parseInt(result[3].slice(2,4), 16);
		const b = parseInt(result[3].slice(4,6), 16);
		if (isNaN(x) || isNaN(y)) {
			return [null, 'One of the coordinates is not a number: ${x} ${y}.'];
		}
		if (isNaN(r) || isNaN(g) || isNaN(b)) {
			return [null, `The color part "${r+g+b}" does not look like a hexadecimal color code.`];
		}

		return [new Instruction(x, y, r, g, b), null];
	}


	constructor(x, y, r, g, b) {
		this.x = x;
		this.y = y;
		this.r = r;
		this.g = g;
		this.b = b;
	}


	toObject() {
		return {
			x: this.x,
			y: this.y,
			r: this.r,
			g: this.g,
			b: this.b
		};
	}


	toString() {
		return `PX ${this.x} ${this.y} ${this.r.toString(16).padStart(2, 0)}${this.g.toString(16).padStart(2, 0)}${this.b.toString(16).padStart(2, 0)}`;
	}
}
