# follows https://nodejs.org/en/docs/guides/nodejs-docker-webapp/
FROM node:15-alpine
ENV NODE_ENV production
RUN apk add --update ffmpeg dumb-init
WORKDIR /usr/src/app
COPY package*.json ./
RUN npm ci --only=production
COPY . .
RUN chown -R node:node /usr/src/app/public/stored
VOLUME ["/usr/src/app/public/stored"]
EXPOSE 8080/tcp
EXPOSE 1234/tcp
USER node
CMD [ "dumb-init", "node", "index.js" ]
