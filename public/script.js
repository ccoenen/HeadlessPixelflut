/* eslint-env browser */
const imagetag = document.querySelector('img');
const stats = document.querySelector('#stats');

async function refreshImage() {
	const response = await fetch('/image.png');
	if (!response.ok) return;
	const blob = await response.blob();
	imagetag.src = URL.createObjectURL(blob);
}

async function refreshStats() {
	const response = await fetch('/stats');
	const data = await response.json();
	if (!response.ok) return;
	stats.innerHTML = `
		Pixels per Second: ${(parseFloat(data.pixelsPerSecond)/1000).toFixed(2)}K<br>
		Active Streams: ${parseInt(data.activeStreams, 10)}<br>
		Total Streams Since Start: ${parseInt(data.totalStreams, 10)}<br>
		Server Uptime: ${(parseFloat(data.uptime) / 3600).toFixed(2)}h<br>
		`;
}

setInterval(refreshImage, 1000);
setInterval(refreshStats, 10000);
refreshStats();

imagetag.addEventListener('mousemove', (e) => {
	console.log(`coord: ${e.clientX}/${e.clientY}`);
});

document.querySelector('#server-name-here').innerText = window.location.hostname;
