# HeadlessPixelflut

Pixelflut is great, but if you can't be in the same room together, you need a headless version!
Implementation of the [Pixelflut protocol](https://wiki.cccgoe.de/wiki/Pixelflut) in JavaScript for rendering to an image canvas.

It comes with a simple web viewer, and it has the ability to stream to any rtmp destination. It will record a timelapse video and/or still images from your Pixelflut-Session.

**I'd be delighted to know if/how this is used, and how it performed! Drop me a line at <headless-pixelflut@wolki.de>**

![HeadlessPixelflut Web-Viewer Screenshot with a large preview image to the left and an explanation what pixelflut is to the right](docs/headless-pixelflut-webfrontend.png)


## Requirements

* Node.JS and npm
* ffmpeg on the PATH


## Setup

There are two main ways to run this, both should perform the same. Choose whatever suits your needs.

In any case, have a look at the `/tools` directory, there are configuration files for other related things in there, too.


### Setup directly on any machine

* Clone this repository: `git clone https://codeberg.org/ccoenen/HeadlessPixelflut.git`
* run `npm ci --only=production` (the more common `npm install` would also work)
* Install ffmpeg. Any version should work fine, just use your distribution's default package.
* Update `config.js` (copy over from `config.js.example`)
* You may need to open up a port in your firewall. Default Pixelflut * port is `1234`. There's a rulefile for `ufw` in `/tools`. Enable with `ufw allow HeadlessPixelflut`.
* run `npm start` or set this up as a service.

We're not dealing with tls. If you wish to use the webserver-part of this project, you should configure a reverse proxy to deliver stuff from port `8080` to your guests.


### Setup with `docker-compose`

* Clone this repository: `git clone https://codeberg.org/ccoenen/HeadlessPixelflut.git`
* Update `docker-compose.yml`.
* Update `config.js` (copy over from `config.js.example`)
* `docker compose up -d` should bring up the service.

Again, you might want to configure a reverse proxy to deliver the http part, if you are using it. Normally the webserver part runs on port `8080`.


## How pixelflut works from a client's perspective

Connect to port 1234/tcp
Write commands in this format:

    PX 20 30 ff8800\n

That is:

- `PX` (always this very string)
- white space
- digits for the x-coordinate
- white space
- digits for the y-coordinate
- white space
- up to eight hexadecimal digits, these will be interpreted as rrggbbaa.
- newline character separates individual commands. If this is `\r\n` it's also fine, we just ignore the `\r`.

In total this is a 14-20 byte message per pixel. This means 1Mbit/s of upstream should be good for 2000-6000 pixels per second (depending on how the packets are segmented).


## How to develop

Make sure to always run the tests via `npm run test` and the linter via `npm run eslint`. If you provide a larger changeset, make sure test-coverage is OK. Tests coverage can be checked with `npm run coverage`.

You can start your own server with `node index.js`.

The `index.js` ties everything together. I try to keep it very short and have most functionality in classes that are in the `lib/` directory.


## Rough block diagram

```
                    ┌────────────────────────────┐
┌──────────┐        │   * Headless Pixelflut *   │
│ Client 1 ├──┐     ├────────────────────────────┤
└──────────┘  │     │                            │
              │     │                            │
┌──────────┐  │     │                            │
│ Client 2 ├──┼─────► Port 1234────┐             │
└──────────┘  │     │              │             │
              │     │              │             │
┌──────────┐  │     │      ┌───────▼──────┐      │
│ Client n ├──┘     │      │              │      │
└──────────┘        │      │              │      │
                    │      │    Canvas    │      │
                    │      │              │      │
                    │      │              │      │
                    │      └─┬──────────┬─┘      │
                    │        │          │        │    ┌─────────────────────┐
                    │        ▼          ▼        │    │   Videostreaming-   │
┌───────────┐       │       PNG       ffmpeg     │    │       Server        │
│ Browser 1 ├─┐     │        │          │        │    │                     │
└───────────┘ │     │        ▼          └────────┼────► RTMP-Port           │
              ├─────► Port 8080                  │    └─────────────────────┘
┌───────────┐ │     │                            │
│ Browser n ├─┘     │                            │
└───────────┘       └────────────────────────────┘
```


## Third Party People

The [original Pixelflut](https://github.com/defnull/pixelflut) is a great project. I would like to thank everyone involved!

This project uses a bunch of npm packages. It would not be possible without the relentless effort of the Free and Open Source community. The package.json/package-lock.json contains a full list. The frontend also uses the font "Concert One" by Johan Kallas and Mihkel Virkus.
