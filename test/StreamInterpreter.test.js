import assert from 'assert';
import { describe, it, beforeEach } from 'mocha';

import MemoryStream from 'memorystream';
import winston from 'winston';

import { logger } from '../lib/logger.js';
import { StreamInterpreter } from '../lib/StreamInterpreter.js';

// silences all the log messages from Server.
logger.clear().add(new winston.transports.Console({level: 'error'}));

describe('StreamInterpreter', () => {
	describe('#constructor', () => {
		it('sets up a linereader', () => {
			const input = MemoryStream(['']);
			const si = new StreamInterpreter('some-id', input);
			assert(si.rl);
			assert(si.stream);
		});
	});

	describe('#setCanvasSize', () => {
		it('sets the canvas size', () => {
			const input = MemoryStream(['']);
			const si = new StreamInterpreter('some-id', input);
			si.setCanvasSize(1920, 1080);
		});
	});

	describe('#processes data from a stream', () => {
		it('fires for an instruction', (done) => {
			const input = MemoryStream();
			const si = new StreamInterpreter('some-id', input);
			si.on('instruction', () => {
				assert(true);
				done();
			});
			input.write('PX 0 0 c0ffee\n');
		});
	});

	describe('reacts to messages correctly', () => {
		let input;
		let si;
		beforeEach(() => {
			input = MemoryStream();
			si = new StreamInterpreter('some-id', input);
		});

		it('size command fires event', (done) => {
			si.once('size', () => {
				done();
			});
			si.line('SIZE');
		});

		it('instruction command fires event', (done) => {
			si.once('instruction', (instruction) => {
				assert.deepStrictEqual(instruction.toObject(), {x: 12, y: 34, r: 0x56, g: 0x78, b: 0x90});
				done();
			});
			si.line('PX 12 34 567890');
		});

		it.skip('out of bounds instruction command fires event', (done) => {
			si.once('unhandled-message', () => {
				done();
			});
			si.line('PX 12000 34000 567890');
		});

		it('gibberish fires event', (done) => {
			si.once('unhandled-message', () => {
				done();
			});
			si.line('asdfa');
		});

		it.skip('too much gibberish fires error', () => {
			si.line('once...');
			si.line('twice...');
			si.line('three times a UNHANDLED ERROR!');
		});
	});

	describe('replies to ', () => {
		it('fires for a size command', (done) => {
			const input = MemoryStream();
			const si = new StreamInterpreter('some-id', input);
			si.on('size', () => {
				assert(true);
				done();
			});
			input.write('SIZE\n');
		});
	});
});
