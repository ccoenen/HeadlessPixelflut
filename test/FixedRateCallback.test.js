import { strictEqual } from 'assert';

import { describe, it } from 'mocha';

import { FixedRateCallback } from '../lib/FixedRateCallback.js';

describe('FixedRateCallback', () => {
	it('fires the expected number of times', (done) => {
		let counter = 0;
		const frcb = new FixedRateCallback(() => {
			counter++;
		}, 20);

		setTimeout(() => {
			frcb.stop();
			strictEqual(counter, 3);
		}, 70);

		setTimeout(() => {
			strictEqual(counter, 3);
			done();
		}, 110);
	});
});
