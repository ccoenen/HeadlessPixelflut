import assert from 'assert';

import { describe, it } from 'mocha';

import { StatsCollector } from '../lib/StatsCollector.js';

const mockServer = {
	on: () => {},
	streams: [],
	streamCounter: 0
};

describe('StatsCollector', () => {
	describe('#constructor', () => {
		it('instanciates', () => {
			const sc = new StatsCollector(mockServer);
			sc.getCurrentStats();
		});
	});

	describe('auto emitter', () => {
		it('fires as often as expected', (done) => {
			const sc = new StatsCollector(mockServer);
			let counter = 0;
			sc.emitStatsInterval(10);
			sc.on('stats', () => {
				counter++;
			});
			setTimeout(() => {
				sc.stopEmitStatsInterval();
				assert.strictEqual(counter, 3);
			}, 35);
			setTimeout(() => {
				assert.strictEqual(counter, 3);
				done();
			}, 55);
		});
	});
});
