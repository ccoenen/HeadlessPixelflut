import assert from 'assert';

import { describe, it } from 'mocha';

import { FilenameGenerator } from '../lib/FilenameGenerator.js';

describe('FilenameGenerator', () => {
	describe('#constructor', () => {
		it('runs', () => {
			const s = new FilenameGenerator('temp/test-output', 'txt');
			assert(s);
		});
	});

	describe('#prepareFilename', () => {
		it('runs without error', () => {
			const s = new FilenameGenerator('temp/test-output', 'txt');
			const name = s.prepareName();
			console.log(name);
		});
	});
});
