import assert from 'assert';
import { describe, it } from 'mocha';
import { Instruction } from '../lib/Instruction.js';

describe('Instruction', () => {
	describe('#constructor', () => {
		it('creates an object with expected values', () => {
			const instruction = new Instruction(5, 10, 11, 22, 33);
			assert.strictEqual(instruction.x, 5);
			assert.strictEqual(instruction.y, 10);
			assert.strictEqual(instruction.r, 11);
			assert.strictEqual(instruction.g, 22);
			assert.strictEqual(instruction.b, 33);
		});
	});

	describe('#fromLine...functions', () => {
		it('should parse a valid string in an expected manner', () => {
			const validInputs = [
				'PX 22 33 112233',
				//'PX 22 33 112233\r',
				//'PX 22 33 112233\n',
				//'PX 22 33 112233\r\n',
			];
			const expected = new Instruction(22, 33, 0x11, 0x22, 0x33);

			[Instruction.fromLineViaRegex, Instruction.fromLineViaSplit].forEach((parser) => {
				validInputs.forEach((input) => {
					const [instruction, _reason] = parser(input);
					assert(instruction, `${parser.name} should have returned an object for "${input}", but returned ${instruction}`);
					assert.strictEqual(instruction.x, expected.x);
					assert.strictEqual(instruction.y, expected.y);
					assert.strictEqual(instruction.r, expected.r);
					assert.strictEqual(instruction.g, expected.g);
					assert.strictEqual(instruction.b, expected.b);
				});
			});
		});

		it('should not return an instruction if it\'s not the right format', () => {
			const invalidInputs = [
				'PX2233112233',
				'X 22 33 112233',
				'0 0 0 0',
				'PX 22 33 asdfgh',
				'PX 22 33 #sdfg;',
				'PX 22 33 fff',
				'PX 22 33 112233 PX 22 33 112233',
			];

			[Instruction.fromLineViaRegex, Instruction.fromLineViaSplit].forEach((parser) => {
				invalidInputs.forEach((input) => {
					const [instruction, _reason] = parser(input);
					assert.strictEqual(instruction, null, `parser ${parser.name} should have returned null for ${input} but returned ${instruction} instead`);
				});
			});
		});
	});

	describe('#toString', () => {
		it('generates the correct string', () => {
			const instructions = [
				'PX 22 33 010203',
				'PX 22 33 315283',
				'PX 22 33 FFFFFF'
			];
			for (const text of instructions) {
				const [instruction, _reason] = Instruction.fromLineViaSplit(text);
				assert.strictEqual(instruction.toString().toUpperCase(), text.toUpperCase());
			}
		});
	});
});
