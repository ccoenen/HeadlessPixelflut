import assert from 'assert';

import { describe, it } from 'mocha';

import { ffmpegStderrStatsToObject, FFMPEG_STDERR_STATS_LINE } from '../lib/VideoOutput.js';

describe('VideoOutput', () => {
	it('matches ffmpeg output lines', () => {
		const actualLines = [
			'frame=    2 fps=1.9 q=0.0 size=       1kB time=00:00:00.00 bitrate=N/A dup=0 drop=23 speed=   0x    ',
			'frame=    2 fps=1.2 q=0.0 size=       1kB time=00:00:00.00 bitrate=N/A dup=0 drop=36 speed=   0x    ',
			'frame=    2 fps=0.9 q=0.0 size=       1kB time=00:00:00.00 bitrate=N/A dup=0 drop=47 speed=   0x    ',
			'frame= 2937 fps= 22 q=29.0 q=29.0 size=   19485kB time=00:01:55.72 bitrate=1379.4kbits/s dup=0 drop=2887 speed=0.875x    ',
			'frame= 2945 fps= 22 q=29.0 q=29.0 size=   20033kB time=00:01:56.04 bitrate=1414.2kbits/s dup=0 drop=2894 speed=0.873x    ',
			'frame= 2962 fps= 22 q=29.0 q=29.0 size=   20048kB time=00:01:56.72 bitrate=1407.1kbits/s dup=0 drop=2911 speed=0.874x    ',
			'frame= 2975 fps= 22 q=29.0 q=29.0 size=   20058kB time=00:01:57.24 bitrate=1401.5kbits/s dup=0 drop=2924 speed=0.874x    ',
			'frame= 2984 fps= 22 q=29.0 q=29.0 size=   20065kB time=00:01:57.60 bitrate=1397.7kbits/s dup=0 drop=2933 speed=0.873x    ',
			'frame=15139 fps= 18 q=30.0 q=27.0 size=  121293kB time=00:10:04.64 bitrate=1643.3kbits/s dup=0 drop=14893 speed=0.739x    ',
			'frame=15167 fps= 19 q=30.0 q=27.0 size=  121313kB time=00:10:05.76 bitrate=1640.6kbits/s dup=0 drop=14921 speed=0.74x    ',
			'frame=15173 fps= 19 q=30.0 q=27.0 size=  121725kB time=00:10:06.00 bitrate=1645.5kbits/s dup=0 drop=14927 speed=0.739x    ',
			'frame=15192 fps= 19 q=30.0 q=27.0 size=  121742kB time=00:10:06.76 bitrate=1643.7kbits/s dup=0 drop=14945 speed=0.74x'
		];

		for (const line of actualLines) {
			assert(line.match(FFMPEG_STDERR_STATS_LINE), `should have matched ${line}`);
		}
	});

	it('doesn\'t match other lines', () => {
		const faultyLines = [
			'abc'
		];

		for (const line of faultyLines) {
			assert(!line.match(FFMPEG_STDERR_STATS_LINE));
		}
	});


	it('deconstructs ffmpeg ouptut correctly', () => {
		const result = ffmpegStderrStatsToObject('frame= 2975 fps= 22 q=29.0 q=29.0 size=   20058kB time=00:01:57.20 bitrate=1401.5kbits/s dup=0 oddthing=N/A drop=2924 speed=0.874x');
		const expected = {
			'ffmpeg.frame': 2975,
			'ffmpeg.fps': 22,
			'ffmpeg.q': 29.0,
			'ffmpeg.size': 20058,
			'ffmpeg.time': 117.20,
			'ffmpeg.bitrate': 1401.5,
			'ffmpeg.dup': 0,
			'ffmpeg.drop': 2924,
			'ffmpeg.speed': 0.874
		};

		assert.deepStrictEqual(result, expected);	
	});


	it('returns null if not an ffmpeg debug output string', () => {
		const result = ffmpegStderrStatsToObject('something something');
		const expected = null;
		assert.deepStrictEqual(result, expected);	
	});
});
