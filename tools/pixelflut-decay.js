import net from 'net';

const PORT = '1234';
const IP = 'localhost';

const client = new net.Socket();

let interval;
let width = 1920;
let height = 1080;
const SIZE_MATCHER = /^SIZE\s+(?<width>\d+)\s+(?<height>\d+)/;

// set port and IP to the correct values (see slides)
client.connect(PORT, IP, function () {
	console.log('connected');

	client.write('SIZE\n');
	interval = setInterval(() => {
		process.stdout.write('.');
		for (let i = 0; i < 10000; i++) {
			client.write(`PX ${Math.floor(Math.random()*width)} ${Math.floor(Math.random()*height)} 000000\n`);
		}
	}, 100);
});

client.on('data', (data)=> {
	const result = data.toString().match(SIZE_MATCHER);
	if (result) {
		width = result.groups.width;
		height = result.groups.height;
		console.log(`SIZE set to ${width}/${height}`);
	} else {
		console.log(`incoming ${data}`);
	}
});
client.on('close', () => {
	clearInterval(interval);
});
client.on('error', (err) => {
	console.error(err);
	client.end();
});
process.on('exit', () => {
	process.stdout.write('\n');
});
