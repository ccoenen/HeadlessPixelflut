import assert from 'assert';

import Benchmark from 'benchmark';

import { Instruction } from '../../lib/Instruction.js';

const input = 'PX 0 0 000000';
const suite = new Benchmark.Suite;

// add tests
suite
	.add('Instruction.fromLineViaRegex', function() {
		assert(Instruction.fromLineViaRegex(input));
	})
	.add('Instruction.fromLineViaSplit', function() {
		assert(Instruction.fromLineViaSplit(input));
	})
	// add listeners
	.on('cycle', function(event) {
		console.log(String(event.target));
	})
	.on('complete', function() {
		console.log('Fastest is ' + this.filter('fastest').map('name'));
	})
// run async
	.run({ 'async': true });
